
package tcp;

import jpcap.PacketReceiver;
import jpcap.packet.Packet;
import jpcap.packet.TCPPacket;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hagos Dess
 */
public class synFloodattackAnalyser implements PacketReceiver {
private int counter=0;
    public synFloodattackAnalyser() {
    }

    @Override
    public void receivePacket(Packet packet) {
        if(packet instanceof TCPPacket){
              TCPPacket pac=(TCPPacket)packet;
                  
          System.out.println("srcPort  "+pac.src_port+"   dstPort     "+pac.dst_port);
           System.out.println("sequence number  "
   +pac.sequence+"ACK number "+pac.ack_num+""+pac.urg+""+pac.ack+"   "+pac.psh+"  "+pac.rst+"   "+pac.syn+"  "
                   +pac.fin+""+pac.rsv1+"  "+pac.rsv2+""+pac.window+"   "+pac.urgent_pointer);
            try {
              
                Connection con=DriverManager.getConnection("jdbc:mysql://localhost/ids","root","");
                //if syn flag is on but ack flag is off which means if there is syn request
                if(pac.syn==true&&pac.ack==false){
                    //holed recored of the attacker
                String sql="insert into synflooding values(?,?,?,?)";
PreparedStatement st=con.prepareStatement(sql);
st.setString(1,pac.src_ip.toString());
st.setString(2,pac.dst_ip.toString());

st.setString(3,"synflooding  attack");
st.setString(4,"2019:3:12");
 st.executeUpdate();
 
 //fetch recored to check if their is an attack
 String sql1="select *from synflooding ";
 PreparedStatement st1=con.prepareStatement(sql1);
 ResultSet r=st1.executeQuery();
 while(r.next()){
   if(counter>100)  {
   System.out.println("Synnflooding attack performed by:"+r.getString(1));
   }
 counter++;
 }
                }
            } catch (SQLException ex) {
                Logger.getLogger(synFloodattackAnalyser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
